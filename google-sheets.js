function getCellUrl(row, col){
    let letter = columnToLetter(col);
    let cellTitle = letter + row;
    // console.log('cellTitle', cellTitle, row, col);
    return `https://docs.google.com/spreadsheets/d/1zOS0qaGY42xtVY1tt26x917E7ljf4pCV3bBCcorytSI/edit#gid=561695598&range=${cellTitle}`;
}

function columnToLetter(column)
{
  var temp, letter = '';
  while (column > 0)
  {
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}

function letterToColumn(letter)
{
  var column = 0, length = letter.length;
  for (var i = 0; i < length; i++)
  {
    column += (letter.charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
  }
  return column;
}

function getCellValue(row, col, entry) {
  var _entry = this.getCell(row, col, entry);

  if (_entry == null) {
      return null;
  }
  return _entry.gs$cell.$t;
}

function getCell(row, col, entry) {
  for (var i = 0; i < entry.length; ++i) {
      var row_a = entry[i].gs$cell.row;
      var _col = entry[i].gs$cell.col;

      if ((row == row_a) && (col == _col)) {
          return entry[i];
      }
  }
  return null;
}