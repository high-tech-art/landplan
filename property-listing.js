class Listing {
    constructor(row, entry) {
        this.entry = entry;
        this.row = row; // basically the id

        this.position = this.getLatLng();
        this.displayPosition = null;    // josh stle nudge so they don't overlap
        this.displayPositionAdjusted = false;
        //console.log('position', this.position);
        //if (this.position == null) { return null; }

        // number attributes
        this.price = this.getNumberAttribute('price');
        this.house_size = this.getNumberAttribute('house_size');
        this.other_buildings_size = this.getNumberAttribute('house_size');
        this.land_area = this.getNumberAttribute('land_area');
        this.folliage = this.getNumberAttribute('folliage');

        // string
        this.listingUrl = this.getStringAttribute('url');
        this.comments_on_buildings = this.getStringAttribute('comments_on_buildings');

        // more complex attributes
        this.votes = this.getVotes();

        // derived attributes
        this.total_building_size = null;
        this.min_vote = this.getfilterMinVoteValue();
        this.max_vote = this.getfilterMaxVoteValue();
        this.average_vote = this.getAverageVoteValue();

    }

    getLatLng() {
        var lat = this.getLat();
        var lng = this.getLng();

        if (lat == null) {
            return null;
        }

        return {
            'lat': Number(lat),
            'lng': Number(lng),
        };

        //return google.maps.LatLng(Number(lat), Number(lng));
    }

    getLat() {
        var value = this.getNumberAttribute('lat');
        if (typeof value === 'number') {
            return value
        }
        return null
    }

    getLng() {
        var value = this.getNumberAttribute('lng');
        if (typeof value === 'number') {
            return value
        }
        return null
    }

    getEditUrl() {
        return `https://docs.google.com/spreadsheets/d/1zOS0qaGY42xtVY1tt26x917E7ljf4pCV3bBCcorytSI/edit#gid=561695598&range=A${this.row}`
    }

    getURL() {
        return this.getNumberAttribute('url');
    }

    getVotes() {
        var votes = []
        for (var i = 0; i < voteMappings.length; ++i) {
            votes.push(this.getVote(i));
        }
        return votes;
    }

    getVote(i) {
        var person = voteMappings[i].person;
        var col = voteMappings[i].col;

        var voteComment = this.getColValue(col);

        var vote = null;
        if (voteComment) {
            vote = voteComment.slice(0, 1);
        }

        var vote = {
            'person': person,
            'vote': vote,
            'comment': voteComment
        };
        return vote
    }

    // todo wrong:
    getfilterMinVoteValue() {
        var values = this.getVoteValues();
        if ((values === null) || (values.length == 0)) {
            return null;
        }
        return Math.min(...values);
    }

    getfilterMaxVoteValue() {
        var values = this.getVoteValues();
        if ((values === null) || (values.length == 0)) {
            return null;
        }
        return Math.max(...values);
    }

    getAverageVoteValue() {
        var values = this.getVoteValues();
        // console.log('vote.values', values);
        if ((values === null) || (values.length == 0)) {
            return null;
        }

        //console.log('-- sum --');
        var sum = 0;
        for (var i = 0; i < values.length; ++i) {
            sum += Number(values[i]);
            // console.log('sum', sum);
        }

        var average = sum/values.length;
        return round(average,1);
    }

    getVoteValue(i) {
        var value = this.votes[i].vote;

        if ((value >= 0) && (value <= voteMaxValue)) {
            return value
        }
        return null;
    }

    getVoteValues() {
        var values = [];
        for (var i = 1; i < this.votes.length; ++i) {
            var value = this.getVoteValue(i);
            if (value === null) {
                continue;
            }
            values.push(value);
        }
        return values;
    }

    getNumberAttribute(attributeKey) {
        var col = data[attributeKey].col;
        var value = this.getColValue(col);
        //console.log('value', value);
        if (value == '?') {
            return value;
        } else if (value == null) {
            return null;
        }
        return Number(value);
    }

    getStringAttribute(attributeKey) {
        var col = data[attributeKey].col;
        return this.getColValue(col);
    }

    // Layer 0 Methods

    getColValue(col) {
        // assumes using this.row, this.entry
        return getCellValue(this.row, col, this.entry);
    }
}

function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}