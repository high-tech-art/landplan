class PropertyListings {

    constructor(entry) {
        // //console.log(entry);

        this.listings = this.getListings(entry);

        // limit for debugging
        // this.listings = this.listings.slice(0, 5);

        this.getDisplayPositions(this.listings);

        //console.log('PropertyListings listings', this.listings);
    }

    getListings(entry) {
        var listings = [];
        for (var row = START_ROW; row < END_ROW; ++row) {
            var listing = new Listing(row, entry);
            if (listing.position != null) {
                listings.push(listing);
            }
        }
        return listings;
    }

    getlistingsRows(listings) {
        var rows = [];
        for (var i = 0; i < listings.length; ++i) {
            rows.push(listings[i].row);
        }
        return rows;
    }

    getDisplayPositions(listings) {
        var rows = this.getlistingsRows(listings);

        // first listing
        listings[0].displayPosition = listings[0].position;

        // rest of listings
        for (var rowToProcess = 1; rowToProcess < rows.length; ++rowToProcess) {

            // default: no adjustment
            listings[rowToProcess].displayPosition = listings[rowToProcess].position;

            var overlaps = this.checkProcessedListingOverlaps(rowToProcess, listings);
            while(overlaps){
                this.adjustDisplayPostion(listings[rowToProcess]);
                overlaps = this.checkProcessedListingOverlaps(rowToProcess, listings);
            }
        }
    }

    checkProcessedListingOverlaps(rowToProcess, listings){
        // all indexes before rowToProcess have been processed
        for (var processedRow = 0; processedRow < rowToProcess; ++processedRow) {
            var overlap = this.checkForOverlap(
                        listings[rowToProcess].displayPosition, 
                        listings[processedRow].displayPosition)
            if (overlap){ return true}
        }
        return false
    }

    checkForOverlap(a, b){
        var distance = calcDistance(a, b);
        
        var overlapThreshold = 2000; // meters
        if (distance < overlapThreshold){
            return true;
        }
        return false
    }

    adjustDisplayPostion(listing){
        listing.displayPositionAdjusted = true;
        listing.displayPosition = {
            'lat': listing.displayPosition.lat + 0.02,
            'lng': listing.displayPosition.lng + 0.02,
        };
    }
};

function calcDistance(from, to) {
    return google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(from.lat, from.lng), 
        new google.maps.LatLng(to.lat, to.lng)
    );
}