// Adds a marker to the map and push to the array.
function newMarker(listing, markers, _status) {

  var marker = new google.maps.Marker({
    title: numberAttributeRpr('price', listing),
    position: listing.displayPosition,
    map: map,
    // status: _status,
    icon: pinSymbol("shown"),
  });

  marker.infowindow = this.newInfowindow(listing);
  marker.listing = listing;

  marker.addListener('click', function () {
    for (var i = 0; i < markers.length; ++i) {
      markers[i].infowindow.close();
    }

    marker.open = false;
    if (!marker.open) {
      marker.infowindow.open(map, marker);
      marker.open = true;
    }
    else {
      marker.infowindow.close();
      marker.open = false;
    }
  });

  return marker;
}


