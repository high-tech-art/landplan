// todo
// - filter property listings 
//     - (dev) without votes per person  
//     - (dev) with missing data
//     - 

// Execute a function when the user releases a key on the keyboard
document.getElementById("filterMinPrice").addEventListener("keyup", enterKey);
document.getElementById("filterMaxPrice").addEventListener("keyup", enterKey);
document.getElementById("filterMinVoteValue").addEventListener("keyup", enterKey);
document.getElementById("filterMaxVoteValue").addEventListener("keyup", enterKey);

function enterKey(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById("updateButton").click();
    }
}

function applyFilters() {
    var filterMinPrice = $('#filterMinPrice').val();
    var filterMaxPrice = $('#filterMaxPrice').val();
    var filterMinVote = $('#filterMinVoteValue').val();
    var filterMaxVote = $('#filterMaxVoteValue').val();
    var filterIncludeNull = $('#filterIncludeNull').serializeArray()[0].value;

    if (filterMinPrice === "") { filterMinPrice = 0 };
    if (filterMaxPrice === "") { filterMaxPrice = 999999 };
    if (filterMinVote === "") { filterMinVote = 0 };
    if (filterMaxVote === "") { filterMaxVote = 3 };

    if ((filterMinPrice < 0) || (filterMinPrice > 999)) {
        filterMinPrice = 0;
    }

    // console.log('@applyFilters', filterMinPrice, filterMaxPrice, filterMinVote, filterMaxVote, filterIncludeNull);

    resetColoursToShown();

    for (var i = 0; i < markers.length; ++i) {
        // console.log('\trow ', markers[i].listing.row);
        
        var anyNullAtrributes = ifAnyNullAtrributes(markers[i]);
        // console.log('anyNullAtrributes', anyNullAtrributes);
        if (anyNullAtrributes){
            markers[i].icon.fillColor = symbolColours['someNull'];
            continue
        }

        var filterOutMinPrice = filter('price', lessThan, filterMinPrice, filterIncludeNull, markers[i]);
        var filterOutMaxPrice = filter('price', greaterThan, filterMaxPrice, filterIncludeNull, markers[i]);
        var filterOutMinVote = filter('min_vote', lessThan, filterMinVote, filterIncludeNull, markers[i]);
        var filterOutMaxVote = filter('max_vote', greaterThan, filterMaxVote, filterIncludeNull, markers[i]);

        if (filterOutMinPrice||filterOutMaxPrice||filterOutMinVote||filterOutMaxVote){
            markers[i].icon.fillColor = symbolColours['hidden'];
        }
    }

    refreshMarkers();
}

function ifAnyNullAtrributes(marker){
    var attributeKeys = ['price', 'min_vote', 'max_vote'];
    for (var i = 0; i < attributeKeys.length; ++i) {
        if (marker.listing[attributeKeys[i]] === null){
            return true;
        }
    }
    return false;
}

function filter(attributeKey, filterFunction, filterValue, filterIncludeNull, marker) {
    
    var attributeValue = marker.listing[attributeKey];
    var row = marker.listing.row;
    // console.log('\n');
    // console.log('@filter.attributeKey', attributeKey);
    // console.log('@filter.attributeValue', attributeValue);
    // console.log('@filter.filterFunction', filterFunction.name);
    // console.log('@filter.filterValue', filterValue);
    // console.log('#filterMinPrice', $('#filterMinPrice').val());
  
    if (attributeValue == null){
        //console.log('@filter.null', row, filterIncludeNull);
        if (filterIncludeNull == 'true') {
            return false;
        } else {
            return true;
        }
    };

    var ff = filterFunction(attributeValue, filterValue);
    // console.log('@filter.ff', ff);
    if (ff) {
        // console.log('filterFunction', filterValue, attributeKey, filterFunction.name, filterIncludeNull, row, );
        return true;
    }
    return false;
}

var lessThan = function (a, b) {
    return a < b;
}

var greaterThan = function (a, b) {
    return a > b;
}

function resetColoursToShown() {
    for (var i = 0; i < markers.length; ++i) {
        markers[i].icon.fillColor = symbolColours['shown'];
    }
}


// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);

    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function refreshMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
        markers[i].setMap(map);
    }
}