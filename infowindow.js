

function htmlVotes(listing) {
    // console.log(listing);
    
    html = '';

    var votes = listing.votes;

    for (var i = 0; i < votes.length; ++i) {
        var person = votes[i].person;
        var vote = votes[i].vote;

        // var comment = votes[i].comment;  // todo:crate a hover over

        var colour = 'lightgrey';
        if (vote != null){
            var colour = voteColours[vote];
        }

        // add 'spacebar' between each vote
        if (i) {
            person = ' ' + person;
        };
        
        var url = getCellUrl(listing.row, voteMappings[i].col);
        html += htmlUrl(url, person, colour);
    }
    return html;
}

function nullEditHtmlUrl(value, row){
    if (attribute == null){
        var editUrl = `https://docs.google.com/spreadsheets/d/1zOS0qaGY42xtVY1tt26x917E7ljf4pCV3bBCcorytSI/edit#gid=561695598&range=A${row}`

    }
}


function infoWindowHtml(listing){

    html = '';
    html += 'url: ' + urlAttributeHtml(listing) + '<br>';
    html += '<br>';
    html += 'price: ' + getNumberAttributeHtmlUrl('price', listing) + '<br>';
    html += 'land: ' + getNumberAttributeHtmlUrl('land_area', listing) + '<br>';
    html += 'house: ' + getNumberAttributeHtmlUrl('house_size', listing) + '<br>';
    html += '<br>';
    html += buildingCommentsAttributeHtml(listing) + '<br>';
    html += '</br>';
    html += htmlVotes(listing) + '<br>';
    html += 'max vote: ' + getNumberAttributeHtmlUrl('max_vote', listing) + '<br>';
    html += 'min vote: ' + getNumberAttributeHtmlUrl('min_vote', listing) + '<br>';
    html += 'average vote: ' + listing.average_vote + '<br>';
    html += '<br>';
    html += 'marker nudged?: ' + listing.displayPositionAdjusted + '<br>';
    html += 'row: ' + getNumberAttributeHtmlUrl('row', listing) + '<br>';

    return html;
}

function newInfowindow(listing) {
    // html
    var contentString = infoWindowHtml(listing);

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    return infowindow;
}