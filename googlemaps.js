// In the following example, markers appear when the user clicks on the map.
// The markers are stored in an array.
// The user can then click an option to hide, show or delete the markers.
var map;
var markers = [];

function initApp(entry){
  var pl = new PropertyListings(entry);

  //console.log(pl.listings.slice(0,2));
  initMap(pl.listings.slice(0,5));
  // initMap(pl.listings);

}

function initMap(listings) {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6.2,
    center: {lat: 43.6625472, lng: 0.5315418}, // spain france border
    mapTypeId: 'terrain'
  });

  map.addListener('click', function(event) {
    // 
  });

  var num = listings.length;  // for limiting number for debugging
  for (var i = 0; i < num; ++i) {
  // Adds markers
    var marker = newMarker(listings[i], markers);
    markers.push(marker);
  }
}