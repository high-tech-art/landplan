SHEET_URL = "https://spreadsheets.google.com/feeds/cells/1dfAeMtXETuaZKuf0At-KU00zx4mN1NmfPPQCLZ0jPSk/1/public/full?alt=json"
// warning: some hardcode values for google sheets url elsewhere

START_ROW = 2;
END_ROW = 200;

data = {
    'row': {
        'col':1,
        'type':'number',
        'prepend': '',
        'append': '',
    },
    'url': {
        'col':2,
        'type':'url',
        'prepend': '',
        'append': '',
    },
    'location url': {
        'col':3,
        'type':'url',
        'prepend': '',
        'append': '',
    },
    'lat': {'col':4},
    'lng': {'col':5},
    'price': {
        'col':6,
        'type':'number',
        'prepend': '€',
        'append': 'k',
    },
    'land_area': {
        'col':10,
        'type':'number',
        'prepend': '',
        'append': ' ha',
    },
    'house_size': {
        'col':11,
        'type':'number',
        'prepend': '',
        'append': ' m²',
    },
    'other_buildings_size': {'col':12},
    'comments_on_buildings': {
        'col':13,
        'type':'string',
        'prepend': '',
        'append': '',
    },
    'folliage': {
        'col':14,
        'type':'number',
        'prepend': '',
        'append': '',
    },
    // derived
    'min_vote': {
        'col':null,
        'type':'number',
        'prepend': '',
        'append': '',
    },
    'max_vote': {
        'col':null,
        'type':'number',
        'prepend': '',
        'append': '',
    },
    'average_vote': {
        'col':null,
        'type':'number',
        'prepend': '',
        'append': '',
    },
    // 'total_building_size': {}, 
}

voteMaxValue = 3;

voteMappings = [
    {'person':'AG', 'col':23}, 
    {'person':'BC', 'col':24},
    {'person':'BD', 'col':25},
    {'person':'DM', 'col':26},
    {'person':'FU', 'col':27}, 
    {'person':'JB', 'col':28},
    {'person':'NW', 'col':29}, 
    {'person':'PC', 'col':30},
    {'person':'SS', 'col':31}, 
    {'person':'TR', 'col':32},
    {'person':'TS', 'col':33},
];

var voteColours = {
    0: 'red',
    1: 'orange',
    2: 'blue',
    3: 'green',
}

var symbolColours = {
    'shown': '#dd2342', //'#006600',
    'someNull': 'grey',
    'hidden': '#eae244',
};




