// conversion of attributes into string representations

function numberAttributeRpr(attributeKey, listing){
    var prependStr = data[attributeKey].prepend;
    var appendStr = data[attributeKey].append;
    
    var value = listing[attributeKey];
    if (value == null){
        var str = 'null'
    } else if (value == '?') {
        var str = prependStr + '?' + appendStr;
    } else {
        var str = prependStr + value + appendStr;
    }
    // console.log(value);
    return str;
}

// needed??
function stringAttributeRpr(attributeKey, listing){
    return numberAttributeRpr(attributeKey, listing);
}

// Specific Attribute HTML URLs

function urlAttributeHtml(listing){
    var attributeKey = 'listingUrl';

    var url = listing[attributeKey];
    //var urlRepr = url.slice(12, 12+24) + '...' + url.slice(-6)
    // console.log('url', urlRepr);

    var colour = numberAttributeUrlColour(attributeKey, listing);
    return htmlUrl(url, url, colour);
}

function buildingCommentsAttributeHtml(listing){
    var attributeKey = 'comments_on_buildings';
    return getNumberAttributeHtmlUrl(attributeKey, listing);
}

// _______________________________________________________

/* function externalUrlAttribute(attributeKey, listing, text){
    var url = listing[attributeKey];
    console.log('url', url);
    var colour = numberAttributeUrlColour(attributeKey, listing);
    return htmlUrl(url, text, colour);
} */

function numberAttributeUrlColour(attributeKey, listing){
    var value = listing[attributeKey];

    if (value == null){
        return 'lightgrey';
    } else if (value == '?') {
        return 'grey';
    } 
    return 'black';
}

function getNumberAttributeHtmlUrl(attributeKey, listing){
    let text = numberAttributeRpr(attributeKey, listing);
    let url = getAttributeCellUrl(attributeKey, listing);
    let colour = numberAttributeUrlColour(attributeKey, listing);
    return htmlUrl(url, text, colour);
}

function getAttributeCellUrl(attributeKey, listing){
    let col = data[attributeKey].col;
    // console.log('@getAttributeCellUrl', col);
    let row = listing.row;
    return getCellUrl(row, col);
}



